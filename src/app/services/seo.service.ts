import { Injectable } from '@angular/core';
import { Meta, MetaDefinition, Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class SeoService {

  constructor(private title: Title, private meta: Meta) { }

  updateTitle(title: string) {
    this.title.setTitle(title);
  }

  updateMetaTags(metaTags: MetaDefinition[]) {
    metaTags.forEach(m => this.meta.updateTag(m));
  }



  updateMetaTag() {
    let metaTags: MetaDefinition[];
    metaTags = [
      { name: 'description', content: 'Game of Thrones Quotes : Winter is Coming, You know nothing Jon Snow, Never forget what you are. The rest of the world will not. Wear it like armour, and it can never be used to hurt you' },
      { property: 'og:title', content: 'GOT Home Page ⚔' },
      { proprety: 'og:description', content: 'Game of Thrones Quotes : Winter is Coming, You know nothing Jon Snow, Never forget what you are. The rest of the world will not. Wear it like armour, and it can never be used to hurt you' },
      { property: 'og:image', content: window.location.href + 'assets/image/homepage.png' },
      { property: 'og:url', content: window.location.href + 'home' },
      { name: "twitter:card", content: "summary_large_image" },
    ]
    metaTags.forEach(m => this.meta.updateTag(m));
  }
}
